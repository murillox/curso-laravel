<?php

use Illuminate\Database\Seeder;
use App\Models\permissions;

class tablepermissionsseeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {   
        factory(permissions::class)->create();

       /*
        Esta forma se usa para crear la cantidad de registro que se quiera hacer
        tan solo cambias el numero 50 por la cantida de registros que quieras hacer

        factory(permissions::class, 50)->create();
       
        Segunda forma para generar registros en times cambias el numero por la 
        cantidad de registros que desen realizar
        factory(permissions::class)->times(50)->create();
        */
    }
}
