<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);
        $this->truncatetables([
            'roles',
            'permissions'
        ]);
        $this->call(tablerolesseeder::class);
        $this->call(tablepermissionsseeder::class);
    }

    protected function truncatetables(array $table){

        DB::statement('SET FOREIGN_KEY_CHECKS = 0;');
        foreach ($table as $table){
            DB::table($table)->truncate();
            # code...
             }
    DB::statement('SET FOREIGN_KEY_CHECKS = 1; ');
    }
        

    
}
